package org.example;

/**
 * @author Justin Calle
 * @date 15.11.2022
 * Komposition:
 * -Applikation besitzt eine Stadt
 * -Applikation besitzt viele Angestellte
 * -Stadt besitzt 5 Wischfahrzeuge
 *
 * Aggregation:
 * -Stadt beschäftigt beliebig viele Angestellte
 * -Angestellter fährt ein oder kein Wischfahrzeug
 */
public class App 
{

    public static void main(String[] args )
    {

        Angestellter[] person = new Angestellter[4];
        person[1] = new Angestellter("Tick");
        person[2] = new Angestellter("Trick");
        person[3] = new Angestellter("Track");
        person[4] = new Angestellter("Donald Duck");

        Stadt winterthur = new Stadt(person);


    }




}

